#!/bin/sh
xrandr --output DisplayPort-0 --mode 1920x1080 --pos 0x0 --rotate left --rate 75 --output DisplayPort-1 --off --output HDMI-A-0 --primary --mode 2560x1440 --pos 1080x189 --rotate normal --rate 144 --output HDMI-A-1 --off --output HDMI-A-1-2 --off --output DisplayPort-1-2 --off
