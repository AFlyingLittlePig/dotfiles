import os
import subprocess
from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy

mod = "mod4"
terminal = "alacritty" 

def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)

def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)

keys = [
    Key([mod], "m", lazy.layout.left(), desc="Move focus to master"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),

    Key([mod], "w", lazy.to_screen(0), desc="Move focus to screen(0)"),
    Key([mod], "e", lazy.to_screen(1), desc="Move focus to screen(1)"),
    Key([mod], "r", lazy.to_screen(2), desc="Move focus to screen(2)"),
    
    Key([mod, "shift"], "m", lazy.layout.swap_left(), desc="Move window to master"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    
    Key([mod], "h", lazy.layout.shrink(), desc="Shrink window"),
    Key([mod], "l", lazy.layout.grow(), desc="Grow window"),
    Key([mod], "space", lazy.layout.reset(), desc="Reset all window sizes"),

    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "c", lazy.window.kill(), desc="Kill focused window"),
    Key([mod], "t", lazy.window.toggle_floating(), desc="Toggle floating"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "p", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),

    Key([mod], "f", lazy.spawn("firefox"), desc="Spawn Firefox"),
    Key([mod], "d", lazy.spawn("thunar"), desc="Spawn Thunar"),
    Key([mod], "s", lazy.spawn("qutebrowser"), desc="Spawn Qutebrowser"),
    Key([mod], "o", lazy.spawn("rofi -show drun"), desc="Spawn Rofi launcher"),
    Key([mod], "v", lazy.spawn("pavucontrol"), desc="Spawn Pavucontrol"),
]

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend(
        [
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
             Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
                 desc="move focused window to group {}".format(i.name)),    
             Key([mod, "shift"], "w", lazy.function(window_to_previous_screen), desc="Move window to previous screen"),
             Key([mod, "shift"], "e", lazy.function(window_to_next_screen), desc="Move window to next screen"),
        ]
    )

layouts = [
    layout.MonadTall(new_client_position="top"),
    layout.MonadWide(new_client_position="top"),
    layout.VerticalTile(),
    layout.Max(),
]

widget_defaults = dict(
    font="Hack",
    fontsize=14,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(),
                widget.Sep(),
                widget.CurrentLayoutIcon(scale=0.7),
                widget.Sep(),
                widget.WindowName(),
                widget.Prompt(),
                widget.Sep(),
                widget.Net(format="UP {up:.0f} {up_suffix} DOWN {down:.0f} {down_suffix}"),
                widget.Sep(),
                widget.CPU(format="CPU {load_percent}%"),
                widget.Sep(),
                widget.Memory(format="RAM {MemPercent}%"),
                widget.Sep(),
                widget.DF(visible_on_warn=False, format="{p} {r:.0f}%"),
                widget.Sep(),
                #widget.PulseVolume(fmt="VOL {}"),
                #widget.Sep(),
                widget.Systray(),
                widget.Sep(),
                widget.Clock(format="%Y-%m-%d %A %H:%M:%S "),
            ],
            30,
            background="#2E3440"
        ),
    ),
    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(),
                widget.Sep(),
                widget.CurrentLayoutIcon(scale=0.7),
                widget.Sep(),
                widget.WindowName(),
                widget.Sep(),
                widget.Clock(format="%Y-%m-%d %A %H:%M:%S "),
            ],
            30,
            background="#2E3440"
        ),
    ),
]

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        *layout.Floating.default_float_rules,
        Match(wm_class="pavucontrol"),
        Match(wm_class="blueberry.py"), 
        Match(wm_class="protonvpn"), 
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

auto_minimize = True

wl_input_rules = None

wmname = "LG3D"

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])
