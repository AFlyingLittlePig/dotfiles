#!/bin/sh

~/.screenlayout/monitor.sh &
picom &
nitrogen --restore &
ibus-daemon -drxR &
xfce4-clipman &
nm-applet &
lxsession &
xfce4-power-manager &
/usr/bin/dunst &
