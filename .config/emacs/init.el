;; Emacs
;; ~/.config/emacs/init.el

;; Customisation 
(load-theme 'wombat t)

(add-to-list 'default-frame-alist
             '(font . "Hack-12"))

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

(global-display-line-numbers-mode 1)
(global-visual-line-mode 1)
(column-number-mode 1)
(save-place-mode 1)

(setq inhibit-startup-message t)
(setq initial-scratch-message nil)
(setq use-dialog-box nil)

;; Moving customisation variables to a separate file and loading it
(setq custom-file (locate-user-emacs-file "custom-vars.el"))
(load custom-file 'noerror 'nomessage)

;; Packages
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

(unless package-archive-contents
 (package-refresh-contents))

;; Initialising use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
   (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

(use-package ivy)
(ivy-mode 1)

(use-package which-key)
(which-key-mode 1)

;; LSP
(use-package eglot)
(add-hook 'python-mode-hook 'eglot-ensure)

;; Org
(add-hook 'org-mode-hook 'flyspell-mode)

(setq org-agenda-files '("~/Documents/Notes/"
			 "~/Documents/Notes/daily/"))
(global-set-key (kbd "C-c a") 'org-agenda)

;; Org-roam
(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory "~/Documents/Notes/")
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n c" . org-roam-capture)
         ;; Dailies
         ("C-c n j" . org-roam-dailies-capture-today))
    :config
  (setq org-roam-node-display-template (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (org-roam-db-autosync-mode))
