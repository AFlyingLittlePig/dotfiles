#!/bin/bash

# Moving the config file of pacman 
sudo cp pacman.conf /etc/

# Updating the system and installing packages
sudo pacman -Syyu
sudo pacman -S --needed - < package_list

# Moving the config files
cp -r .config ~/
cp -r .screenlayout ~/
sudo cp environment /etc/
sudo cp 50-mouse-acceleration.conf /etc/X11/xorg.conf.d/
sudo cp 81-bluetooth-hci.rules /etc/udev/rules.d/
sudo cp 20-protonvpn.rules /etc/polkit-1/rules.d/

chmod +x ~/.config/qtile/autostart.sh
chmod +x ~/.screenlayout/monitor.sh

# lightdm
sudo systemctl enable lightdm.service

# ufw
sudo ufw default allow outgoing
sudo ufw default deny incoming
sudo ufw allow 25,53,67,68,80,123,443/tcp
sudo ufw allow 25,53,67,68,80,123,443/udp
sudo ufw allow 27000:27100/udp
sudo ufw allow 3478,4379,4380/udp
sudo ufw allow 27015:27050/tcp
sudo ufw enable
sudo systemctl enable ufw.service

# blueberry
sudo systemctl enable bluetooth

# AUR
# yay
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si

cd ..

# Installing AUR packages
yay -S --needed - < aur_package_list

# timeshift
sudo systemctl enable cronie.service

# VM
# virt-manager
sudo pacman -S --needed virt-manager qemu-desktop libvirt edk2-ovmf dnsmasq vde2 bridge-utils gnu-netcat dmidecode
sudo cp libvirtd.conf /etc/libvirt/
sudo cp qemu.conf /etc/libvirt/
sudo systemctl enable --now libvirtd.service
sudo usermod -a -G libvirt $(whoami)
sudo virsh net-autostart default

reboot
